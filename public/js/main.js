document.addEventListener('DOMContentLoaded', () => {
    calcOrderPrice();
});

function calcOrderPrice() {
    document.getElementById('orderValue').addEventListener('click', () => {
        const price = document.getElementById('price').value;
        const quantity = document.getElementById('quantity').value;
        const output = document.getElementById('output');

        if (price.match(/[0-9]/i) === null || quantity.match(/[0-9]/i) === null)
            alert('Введите корректные данные');
        else output.innerHTML = 'Цена зказа  =  ' + price * quantity;

    });
}